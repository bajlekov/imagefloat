ImageFloat
==========

![ImageFloat](https://raw.githubusercontent.com/bajlekov/ImageFloat/master/doc/source/preview.png)

Image processing framework and user interface for definition and execution of flexible processing pipelines. Focus on parallel processing, GPU acceleration, and real-time preview.

Documentation
-------------
_Work in progress_

[https://imagefloat.readthedocs.io/en/latest/](https://imagefloat.readthedocs.io/en/latest/)

Releases
--------
[https://github.com/bajlekov/ImageFloat/releases](https://github.com/bajlekov/ImageFloat/releases)

Source code
-----------
[https://github.com/bajlekov/ImageFloat](https://github.com/bajlekov/ImageFloat)
